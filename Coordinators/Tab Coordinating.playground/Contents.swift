import PlaygroundSupport
import UIKit

protocol Coordinator: class {
    func start()
}

protocol TabCoordinator: Coordinator {
    var viewController: UIViewController { get }
    var navigationController: UINavigationController { get }
    var tabBarItem: UITabBarItem { get }
}

extension TabCoordinator {
    func start() {
        navigationController.tabBarItem = tabBarItem
        navigationController.pushViewController(viewController, animated: false)
    }
}

final class HomeCoordinator: TabCoordinator {
    var tabBarItem = UITabBarItem(title: "Home", image: nil, selectedImage: nil)
    var navigationController = UINavigationController()
    var viewController: UIViewController = {
        $0.title = "Home"
        return $0
    }(UIViewController())
}

final class AppCoordinator: Coordinator {
    let tabBarController: UITabBarController
    private var childCoordinators: [TabCoordinator]  = [
        HomeCoordinator()
    ]
    
    init(tabBarController: UITabBarController) {
        self.tabBarController = tabBarController
    }
    
    func start() {
        var viewControllers: [UIViewController] = []
        childCoordinators.forEach { tabCoordinator in
            tabCoordinator.start()
            viewControllers.append(tabCoordinator.navigationController)
        }
        
        tabBarController.viewControllers = viewControllers
    }
}

let tabBar = UITabBarController()
let app = AppCoordinator(tabBarController: tabBar)
app.start()

PlaygroundPage.current.liveView = tabBar
PlaygroundPage.current.needsIndefiniteExecution = true
